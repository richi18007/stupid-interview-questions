#include<iostream>
#include<string>
#include<vector>
#include<map>
#include<queue>
#include<utility>

using namespace std ;

string document ; 
vector< string > tokens_document ; 
std::vector<string> search_keywords ;
const char *tokens = " \t\n" ; 
map<string , int> count_hash ;
map<string , int> keyword_hash ; 
queue< pair<int , int> > words_queue ; 
const int  input_document_size = 10000 ; // Feel free to play with this value 
char input_document[input_document_size] ; //Used for parsing input data

void get_input_data() 
{

	int num_search_keywords ; 
	cin>>document ; 
	cin>>num_search_keywords ; 
	for (int i = 0; i < num_search_keywords; ++i)
	{
		string search_keyword ; 
		cin>>search_keyword ; 
		search_keywords.push_back(search_keyword) ; 
	}
}

/*
 * Tokenize the input document on the basis of tokens ( space / tabs /newlines) 
 */
void tokenize_input_data()
{
	const char * temp_token = document.c_str() ; 
	// Free to play around with this value  
	// The document should have multiple tokens seperated by spaces 
	strcpy(input_document , temp_token) ; 
	char *token_document = strtok(input_document , tokens );
	while (token_document) 
	{
		tokens_document.push_back(string(token_document)) ; 
    	token_document = strtok(NULL, " ") ;
	}
}
// i is index.
void update_position_document(int i  , int &words_found )
{
	string token = string(tokens_document[i]) ; 
	if(keyword_hash.find(token) != keyword_hash.end())
	{
		words_found++ ; 
		// Compiler's job to optimize two gets
		pair<int , int> position_word(i ,keyword_hash[token] ) ; 
		words_queue.push(position_word) ; 
	}
	return ; 
}
/*
 * Finds the minimum window in the document 
 * Keep a FIFO queue with all the elements when you have the
 * first slice
 */
pair<int , int > find_minimum_window()
{	
	int words_found , i;
	int minimum_window_left  , minimum_window_right  , minimum_window_size ;
	/*
     * Note : Used keyword.size() twice. The compiler should
     * take care of these kinds of petty , domain independent
     * optimizations . And if someone tries to optimize these
     * kinds of things , then they should take a course on com-
     * piler design and code clarity 
	 */
	for (i = 0 , words_found = 0 ; i < tokens_document.size() &&
		words_found<tokens_document.size(); ++i)
	{
		update_position_document(i , words_found) ; 
	}
	if (words_found == tokens_document.size())
	{
		minimum_window_left = words_queue.front().first ; 
		minimum_window_right= i ; 
		minimum_window_size = minimum_window_right - minimum_window_left ; 
		/* 
		   We've found the first sequence. Now all we need to do
		   is the following :-
	
			1. Scan the remaining tokens 
			2. When you see a matching word , push it to the queue
			3. If the matching word is going to be the same as the
			   first word in the queue, remove the first word and 
			   check the size of the queue again 
			4. Keep a global minima and keep updating the same 
		 */
		for( ; i<tokens_document.size() ; i++ )
		{
			string token = string(tokens_document[i]) ; 
			if(keyword_hash.find(token) != keyword_hash.end())
			{
				pair<int , int > position_current_token(i , keyword_hash[token] ) ; 
				string front_element = tokens_document[words_queue.front().second] ; 
				if( front_element == token )
				{
					words_queue.pop() ; 
					words_queue.push(position_current_token) ; 
					// Update global minimum window size 
					int current_window_left = words_queue.front().first ; 
					int current_window_right= i ; 
					int current_window_size = current_window_right - current_window_left ; 
					if(current_window_size < minimum_window_size)
					{
						minimum_window_left = current_window_left ;
						minimum_window_right = current_window_right ;
						minimum_window_size = current_window_size ; 
					}
				}
				else 
				{
					words_queue.push(position_current_token) ; 
				}
			}
		}	
	}
	return pair<int , int>(minimum_window_left , minimum_window_right) ; 
}

void build_search_keyword_hash()
{
	for (int i = 0; i < search_keywords.size( ); ++i)
	{
		/* code */
		string keyword = string(search_keywords[i]) ; 
		keyword_hash[keyword] = i ; 
	}
}
int main()
{
	get_input_data() ;  
	tokenize_input_data() ; 
	build_search_keyword_hash() ; 
	pair<int , int> window = find_minimum_window() ; 
	cout<<"The window size is "<<window.first<<" to "<<window.second<<endl;
}